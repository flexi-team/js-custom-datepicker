'use strict';

import {jsCustomDatepicker} from './plugin/index';

(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define([], () => {
            return (root.jsCustomDatepicker = factory());
        });
    } else if (typeof exports === 'object') {
        module.exports = factory();
    } else {
        root.jsCustomDatepicker = factory();
    }
}(window, function () {

    const root = window;

    if (root.jQuery || root.Zepto) {
        (function () {
            $.fn.jsCustomDatepicker = params => {
                return this.each(() => {
                    $(this).data('jsCustomDatepicker', new jsCustomDatepicker($(this)[0], params || []));
                });
            };
        })(root.jQuery || root.Zepto);
    }

    return jsCustomDatepicker;
}));


