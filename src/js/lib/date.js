'use strict';

if (!Date.prototype.format) {

    Date.prototype.getMonthName = function () {
        let month_names = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        ];

        return month_names[this.getMonth()];
    };

    Date.prototype.getMonthAbbr = function () {
        let month_abbrs = [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec'
        ];

        return month_abbrs[this.getMonth()];
    };

    Date.prototype.getDayFull = function () {
        let days_full = [
            'Sunday',
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday'
        ];
        return days_full[this.getDay()];
    };

    Date.prototype.getDayAbbr = function () {
        let days_abbr = [
            'Sun',
            'Mon',
            'Tue',
            'Wed',
            'Thur',
            'Fri',
            'Sat'
        ];
        return days_abbr[this.getDay()];
    };

    Date.prototype.getDayOfYear = function () {
        let onejan = new Date(this.getFullYear(), 0, 1);
        return Math.ceil((this - onejan) / 86400000);
    };

    Date.prototype.getDaySuffix = function () {
        let d = this.getDate();
        let sfx = ["th", "st", "nd", "rd"];
        let val = d % 100;

        return (sfx[(val - 20) % 10] || sfx[val] || sfx[0]);
    };

    Date.prototype.getWeekOfYear = function () {
        let onejan = new Date(this.getFullYear(), 0, 1);
        return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
    };

    Date.prototype.isLeapYear = function () {
        let yr = this.getFullYear();

        if ((parseInt(yr) % 4) === 0) {
            if (parseInt(yr) % 100 === 0) {
                if (parseInt(yr) % 400 !== 0) {
                    return false;
                }
                if (parseInt(yr) % 400 === 0) {
                    return true;
                }
            }
            if (parseInt(yr) % 100 !== 0) {
                return true;
            }
        }
        if ((parseInt(yr) % 4) !== 0) {
            return false;
        }
    };

    Date.prototype.getMonthDayCount = function () {
        let month_day_counts = [
            31,
            this.isLeapYear() ? 29 : 28,
            31,
            30,
            31,
            30,
            31,
            31,
            30,
            31,
            30,
            31
        ];

        return month_day_counts[this.getMonth()];
    };

    Date.prototype.format = function (dateFormat) {

        let _dateFormat = dateFormat || 'dd-mm-YYYY';



        dateFormat = _dateFormat.split("");

        let date = this.getDate(),
                month = this.getMonth(),
                hours = this.getHours(),
                minutes = this.getMinutes(),
                seconds = this.getSeconds();
        let date_props = {
            d: date < 10 ? '0' + date : date,
            D: this.getDayAbbr(),
            j: this.getDate(),
            l: this.getDayFull(),
            S: this.getDaySuffix(),
            w: this.getDay(),
            z: this.getDayOfYear(),
            W: this.getWeekOfYear(),
            F: this.getMonthName(),
            m: month < 9 ? '0' + (month + 1) : month + 1,
            M: this.getMonthAbbr(),
            n: month + 1,
            t: this.getMonthDayCount(),
            L: this.isLeapYear() ? '1' : '0',
            Y: this.getFullYear(),
            y: this.getFullYear() + ''.substring(2, 4),
            a: hours > 12 ? 'pm' : 'am',
            A: hours > 12 ? 'PM' : 'AM',
            g: hours % 12 > 0 ? hours % 12 : 12,
            G: hours > 0 ? hours : "12",
            h: hours % 12 > 0 ? hours % 12 : 12,
            H: hours,
            i: minutes < 10 ? '0' + minutes : minutes,
            s: seconds < 10 ? '0' + seconds : seconds
        };

        let date_string = "";

        let prev_key = '';
        for (let i = 0; i < dateFormat.length; i++) {

            let f = dateFormat[i];

            if (prev_key === f) {
                continue;
            }
            if (f.match(/[a-zA-Z]/g)) {
                date_string += date_props[f] ? date_props[f] : '';
            } else {
                date_string += f;
            }
            prev_key = f;
        }




        return date_string;
    };
}

if (!Date.prototype.adjustDate) {
    Date.prototype.adjustDate = function (days) {
        let date;

        days = days || 0;


        if (days === 0) {
            date = this.getTime();
        } else if (days > 0) {
            date = this.getTime() + Math.abs(days * 86400000);
        } else {
            date = this.getTime() - Math.abs(days * 86400000);
        }

        this.setTime(date);
        this.setHours(0);

        return this;

    };
}