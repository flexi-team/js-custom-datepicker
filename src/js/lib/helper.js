'use strict';

export const clone = obj => {
    if (!obj) {
        return null;
    } else if (obj.constructor === Array) {
        return obj.map(clone);
    } else if (typeof obj === 'object') {
        let result = {};
        for (var key in obj) {
            result[key] = clone(obj[key]);
        }
        return result;
    } else {
        return obj;
    }
};

export const extend = (original, source) => {
    var result = clone(original);
    for (let key in source) {
        result[key] = clone(source[key]);
    }
    return result;
};

export const browser = {
    addEventListener: !!window.addEventListener,
    touch: ('ontouchstart' in window) || window.DocumentTouch && document instanceof window.DocumentTouch
};