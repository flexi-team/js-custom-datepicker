'use strict';

if (!Element.prototype.closest) {
    Element.prototype.closest = function (css) {
        var node = this;

        while (node) {
            if (node.matches(css))
                return node;
            else
                node = node.parentElement;
        }
        return null;
    };
}

if (!Element.prototype.matches) {
    Element.prototype.matches = Element.prototype.matchesSelector || Element.prototype.webkitMatchesSelector || Element.prototype.mozMatchesSelector || Element.prototype.msMatchesSelector;
}

const DOM = {};

DOM.createTag = (tag, attrs) => {
    let _tag = DOM.e(tag);
    if (attrs instanceof Object && Object.keys(attrs).length) {
        for (let key in attrs) {
            _tag.setAttribute(key, attrs[key]);
        }
    }

    return _tag;
};

DOM.e = (tagName, className) => {
    let element = document.createElement(tagName);
    if (className) {
        element.className = className;
    }
    return element;
};

DOM.remove = element => {
    if (typeof element.remove !== 'undefined') {
        element.remove();
    } else {
        if (element.parentNode) {
            element.parentNode.removeChild(element);
        }
    }
};

export default DOM;