'use strict';

const oldAdd = (element, className) => {
    let classes = element.className.split(' ');
    if (classes.indexOf(className) < 0) {
        classes.push(className);
    }
    element.className = classes.join(' ');
};

const oldRemove = (element, className) => {
    let classes = element.className.split(' ');
    let idx = classes.indexOf(className);
    if (idx >= 0) {
        classes.splice(idx, 1);
    }
    element.className = classes.join(' ');
};

export const add = (element, className) => {
    
    if (element.classList) {
        element.classList.add(className);
    } else {
        oldAdd(element, className);
    }
};

export const remove = (element, className) => {
    if (element.classList) {
        element.classList.remove(className);
    } else {
        oldRemove(element, className);
    }
};

export const list = (element) => {
    if (element.classList) {
        return Array.prototype.slice.apply(element.classList);
    } else {
        return element.className.split(' ');
    }
};