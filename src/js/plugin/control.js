'use strict';

import DOM from '../lib/dom';
import * as i18n from './i18n';
import * as cls from '../lib/class';

export const Control = (options) => {

    const select_months = (selectAttr) => {

        let _select = DOM.createTag('select', selectAttr);

        for (let month = 1; month <= 12; month++) {

            let attrs = {
                value: new Date(options.current_year, month - 1, 1, 0, 0, 0, 0).format(options.dateFormat)
            };
            if (month === options.current_month) {
                attrs.selected = 'selected';
            }

            let _options = DOM.createTag('option', attrs);
            let _lg = typeof i18n[options.language] === 'object' ? i18n[options.language] : i18n.de;

            _options.innerHTML = _lg.month['month.' + month];

            _select.onchange = options.getDate;
            _select.appendChild(_options);
        }

        return _select;
    };

    const select_years = (selectAttr, years) => {

        let _select = DOM.createTag('select', selectAttr);

        if (typeof years === 'undefined' || typeof years.from === 'undefined') {
            years.from = options.current_year - 10;
        } else if (!options.minYear) {
            options.minYear = years.from;
        }

        if (typeof years === 'undefined' || typeof years.to === 'undefined') {
            years.to = options.current_year + 10;
        } else if (!options.maxYear) {
            options.maxYear = years.to;
        }

        if (parseInt(years.from) > parseInt(years.to)) {
            return _select;
        }

        let year = years.to;
        while (year >= years.from) {
            let attrs = {
                value: new Date(year, options.current_month - 1, 1, 0, 0, 0, 0).format(options.dateFormat)
            };
            if (year === options.current_year) {
                attrs.selected = 'selected';
            }

            let _options = DOM.createTag('option', attrs);
            _options.innerHTML = year;

            _select.onchange = options.getDate;
            _select.appendChild(_options);
            year--;
        }

        return _select;
    };

    const prev = () => {

        let el = DOM.createTag('span', {
            'class': 'datepicker-prev text-left',
            'data-date': new Date(options.current_year, options.current_month - 2, 1, 0, 0, 0, 0).format(options.dateFormat)
        });

        if (options.minYear) {
            if (new Date(options.current_year, options.current_month - 1, 0, 0, 0, 0).getFullYear() >= options.minYear) {
                el.onclick = options.getDate;
            } else {
                cls.add(el, 'disabled');
            }
        } else {
            el.onclick = options.getDate;
        }

        el.innerHTML = '<i class="fa fa-caret-left" aria-hidden="true"></i>';

        return el;
    };

    const next = () => {

        let el = DOM.createTag('span', {
            'class': 'datepicker-next text-left',
            'data-date': new Date(options.current_year, options.current_month, 1, 0, 0, 0, 0).format(options.dateFormat)
        });

        if (options.maxYear) {
            if (new Date(options.current_year, options.current_month, 1, 0, 0, 0, 0).getFullYear() <= options.maxYear) {
                el.onclick = options.getDate;
            } else {
                cls.add(el, 'disabled');
            }
        } else {
            el.onclick = options.getDate;
        }

        el.innerHTML = '<i class="fa fa-caret-right" aria-hidden="true"></i>';

        return el;
    };

    const title = () => {

        let title = DOM.createTag('div', {'class': 'datepicker-title clearfix'});
        let div_months = DOM.createTag('div', {'class': 'datepicker-month'});

        let months_list = select_months({
            'class': "js-custom-select datepicker-select-month",
            'data-type': 'MonthsList'
        });

        div_months.appendChild(months_list);
        title.appendChild(div_months);

        if (options.changeYear) {

            let select_list = select_years({
                'class': "js-custom-select datepicker-select-year",
                'data-type': 'YearsList'
            }, options.years || {});

            let div_years = DOM.createTag('div', {
                'class': 'datepicker-year'
            });

            div_years.appendChild(select_list);
            title.appendChild(div_years);
        }

        return title;
    };

    let header = DOM.createTag('div', {'class': 'datepicker-header'});
    header.appendChild(prev());
    header.appendChild(title());
    header.appendChild(next());

    return header;

};