'use strict';

export const Default = {
    dateFormat: 'dd-mm-YYYY',
    autoclose: true,
    dragAutoClose: 200,
    changeYear: true,
    width: 300,
    binderID: false,
    between: false,
    language: 'en',
    select: {
        "max-height": 200,
    }
};
