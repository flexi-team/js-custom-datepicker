'use strict';

export const de = {
    month: {
        'month.1': 'Januar',
        'month.2': 'Februar',
        'month.3': 'März',
        'month.4': 'April',
        'month.5': 'Mai',
        'month.6': 'Juni',
        'month.7': 'Juli',
        'month.8': 'August',
        'month.9': 'September',
        'month.10': 'Oktober',
        'month.11': 'November',
        'month.12': 'Dezember',
        'month.short.1': 'Jan.',
        'month.short.2': 'Feb.',
        'month.short.3': 'März',
        'month.short.4': 'Apr.',
        'month.short.5': 'Mai',
        'month.short.6': 'Juni',
        'month.short.7': 'Juli',
        'month.short.8': 'Aug.',
        'month.short.9': 'Sep.',
        'month.short.10': 'Okt.',
        'month.short.11': 'Nov.',
        'month.short.12': 'Dez.'
    },
    dayNames: {
        short: {
            1: 'Mo',
            2: 'Di',
            3: 'Mi',
            4: 'Do',
            5: 'Fr',
            6: 'Sa',
            7: 'So'
        }
    }
};


export const en = {
    month: {
        'month.1': 'January',
        'month.2': 'February',
        'month.3': 'March',
        'month.4': 'April',
        'month.5': 'May',
        'month.6': 'June',
        'month.7': 'July',
        'month.8': 'August',
        'month.9': 'September',
        'month.10': 'October',
        'month.11': 'November',
        'month.12': 'December',
        'month.short.1': 'Jan.',
        'month.short.2': 'Feb.',
        'month.short.3': 'März',
        'month.short.4': 'Apr.',
        'month.short.5': 'Mai',
        'month.short.6': 'Juni',
        'month.short.7': 'Juli',
        'month.short.8': 'Aug.',
        'month.short.9': 'Sep.',
        'month.short.10': 'Okt.',
        'month.short.11': 'Nov.',
        'month.short.12': 'Dez.'
    },
    dayNames: {
        short: {
            1: 'Mo',
            2: 'Di',
            3: 'Mi',
            4: 'Do',
            5: 'Fr',
            6: 'Sa',
            7: 'So'
        }
    }
};

