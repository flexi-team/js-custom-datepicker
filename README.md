# README #

## Description: ##

* Сalendar was written on javascript using es6 format and was redone to es5 using webpack and babel tools. There are many classes for easy styling for the desired website design.
* You can review the example here: - [Demo](http://js-custom-datepicker.flexi.ink)

-----------------------------------------------------------

## Install

###You can install it from npm:

```
npm install js-custom-datepicker --save
```

or download from git
```
git clone https://bitbucket.org/flexi-team/js-custom-datepicker.git
```

### To use all capabilities of Сalendar, you need include [js-custom-select](https://www.npmjs.com/package/js-custom-select) and [js-custom-scroll](https://www.npmjs.com/package/js-custom-scroll)

```
npm install js-custom-scroll --save
npm install js-custom-select --save
```

or download from git
```
git clone https://bitbucket.org/flexi-team/js-custom-scroll.git
git clone https://bitbucket.org/flexi-team/js-custom-select.git
```

# Usage

### Include necessary style:

```html
<link rel="stylesheet" href="/dist/js-custom-datepicker.css"/>
```

### Html Code:

```html
<input readonly id="inputForStatic" class="datePicker-input" type="text"/>
<div id="datePickerStatic" ></div>
```

### Include necessary script:

```html
<script async src="/dist/js-custom-datepicker.min.js" onload="InitDatePicker()"></script>
```

### JavaScript Code:

```JavaScript
<script type="text/javascript">
    function InitDatePicker() {
        var static = new jsCustomDatepicker(document.getElementById('datePickerStatic'), {
            between: true,
            minDate: new Date().format('dd-mm-YYYY'),
            years: {
                to: 2019,
                from: 2017,
            },
            select: {
                "max-height": 300,
                scroll: {
                    draggable: true,
                }
            },
            onSelect: function (input, date) {
                document.getElementById('inputForStatic').value = date.from + ' - ' + date.to;
            }
        });
        console.log(static);
    }
</script>
```

### Properties:

* dateFormat {String} - Format of the sent date. Defaults - `dd-mm-YYYY`
* onSelect {Function} - The function to be executed after date selection.
* width {Number} - Calendar width.
* minDate {Boolean || String} - Defaults to `false`, You can specify min. Date.
* maxDate {Boolean || String} - Defaults to `false`, You can specify the max. Date.
* changeYear {Boolean} - Defaults to `true`, you can pass in `false` to disable component.
* autoclose {Boolean} - Defaults to `true`, you can pass in `false` to disable component.
* years: {
    from: minYear,
    to: maxYear
} - Set the year range.
* select: {object} - Sets options for custom-select.
* select['max-height'] - Defaults to `200`px.
* binderID {Boolean || String} - Defaults to `false`, You can specify the id of the connected input to connect dates.
* between {Boolean} - Defaults to `true`, you can pass in `false` to disable component. The range should be set in data-between. Example:
```html
<input readonly id="datePickerInput" type="text" data-between='{"from":"07-06-2017", "to":"14-06-2017"}'/>
```
* dragAutoClose {Number} - Delay time before closing.

### You can specify options for the element directly. At first, those will be used which were sent during the initialization, and then these will be applied. They have the biggest priority. Example:
```html
<input id="datePickerInput" data-options='{"binderID":"date-to"}' />
```